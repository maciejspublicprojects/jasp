#!/bin/bash

PID=$1
COMMAND=${2:-start}
THISPATH=$(dirname "$0")
ROOTPATH="$THISPATH/../.."

#DEBUG="-agentlib:jdwp=transport=dt_socket,server=n,address=localhost:5005,suspend=y"
CPPATH="$ROOTPATH/build/libs/*:$ROOTPATH/libs/*"

cat $THISPATH/agent_config.yml \
  | sed "s/_PID_HERE_/$PID/g" \
  | sed "s/_COMMAND_/$COMMAND/g" \
  | java $DEBUG -cp "$CPPATH" com.itfruit.jmcutils.JavaAttachSampler
