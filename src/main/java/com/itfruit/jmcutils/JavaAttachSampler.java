package com.itfruit.jmcutils;

import com.itfruit.jmcutils.attach.AttachedSampler;
import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.config.ConfigReader;
import com.sun.tools.attach.VirtualMachine;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class JavaAttachSampler {
    private static AtomicReference<AttachedSampler> attachedSamplerReference = new AtomicReference<>();
    private static ConfigReader configReader = new ConfigReader();

    public static void main(String[] args) throws Exception {
        var config = configReader.readYamlFromInput(System.in, AgentConfig.class);

        var agentLocation = new File(JavaAttachSampler.class.getProtectionDomain().getCodeSource().getLocation()
                .toURI()).getPath();

        System.out.println(String.format("Injecting agent: %s", agentLocation));

        var vm = VirtualMachine.attach(config.getPid().toString());
        try {
            vm.loadAgent(agentLocation, configReader.toStringYaml(config));
        } finally {
            vm.detach();
        }
    }

    public static void premain(String args) {
        agentmain(args);
    }

    public static void agentmain(String args) {
        AgentConfig config;
        try {
            config = configReader.readYamlFromInput(new ByteArrayInputStream(args.getBytes()), AgentConfig.class);
        } catch (IOException e) {
            throw new RuntimeException("Agent Sampler failed to initialize", e);
        }

        var attachedSampler = attachedSamplerReference.get();
        if (attachedSampler != null) {
            if (config.getCommand().equals(AgentConfig.Command.start)) {
                throw new RuntimeException("Agent Sampler already started");
            } else { // stop or restart
                attachedSampler.shutDownAll();
                attachedSamplerReference.set(null);
            }
        }

        if (config.getCommand().equals(AgentConfig.Command.stop)) {
            return;
        }

        attachedSampler = new AttachedSampler(config);
        attachedSamplerReference.set(attachedSampler);
    }
}
