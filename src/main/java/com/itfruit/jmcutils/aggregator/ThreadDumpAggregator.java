package com.itfruit.jmcutils.aggregator;

import com.itfruit.jmcutils.config.AgentConfig;
import com.itfruit.jmcutils.executors.CatchingRunnable;
import com.itfruit.jmcutils.executors.ScheduledExecutorFactory;
import com.itfruit.jmcutils.results.FlameGraphFlatResultsWriter;
import com.itfruit.jmcutils.thread.ThreadAndStackTrace;
import com.itfruit.jmcutils.tree.TreeWithValue;

import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.regex.Matcher;

public class ThreadDumpAggregator {
    private final ScheduledExecutorService threadDumpAggregatorExecutor = ScheduledExecutorFactory.setupScheduledPool("Thread Dump Aggregator");
    private final AgentConfig agentConfig;
    private final Supplier<List<ThreadAndStackTrace>> dumpSupplier;
    private final FlameGraphFlatResultsWriter resultsWriter;
    private final BlockingQueue<TreeWithValue<String, BigInteger>> treeQueue;
    private final AtomicReference<ThreadAndStackTrace> lastThreadDumpElement = new AtomicReference<>();

    public ThreadDumpAggregator(AgentConfig agentConfig, Supplier<List<ThreadAndStackTrace>> dumpSupplier) {
        this.agentConfig = agentConfig;
        this.dumpSupplier = dumpSupplier;
        resultsWriter = new FlameGraphFlatResultsWriter(agentConfig);

        treeQueue = new ArrayBlockingQueue<>((int) Duration.ofMinutes(1).toSeconds());

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        threadDumpAggregatorExecutor.scheduleAtFixedRate(new CatchingRunnable(this::aggregatorPrinter), 1,
                agentConfig.getIntervals().getAggregateDumpsToResults().getSeconds(), TimeUnit.SECONDS);
    }

    public void shutdown() {
        threadDumpAggregatorExecutor.shutdownNow();
    }

    public List<TreeWithValue<String, BigInteger>> retrieveTrees() {
        var list = new LinkedList<TreeWithValue<String, BigInteger>>();
        treeQueue.drainTo(list);
        return list;
    }

    private void aggregatorPrinter() throws InterruptedException {
        var threadDumps = dumpSupplier.get();

        // Cannot do a diff with just one element
        if (threadDumps.size() < 2) {
            return;
        }

        // Re-add threadDump from last aggregation - so we won't be loosing a diff between last and first of "next round" of aggregation.
        if (lastThreadDumpElement.get() != null) {
            threadDumps.add(0, lastThreadDumpElement.get());
        }
        lastThreadDumpElement.set(threadDumps.get(threadDumps.size() - 1));

        var tree = aggregateGraph(threadDumps);

        treeQueue.put(tree);

        var path = Path.of("results-" + agentConfig.getIntervals().getAggregateDumpsToResults().toString());
        resultsWriter.writeToFile(tree, path);
    }

    private TreeWithValue<String, BigInteger> aggregateGraph(List<ThreadAndStackTrace> data) {
        var rootTree = new TreeWithValue<String, BigInteger>();

        ThreadAndStackTrace lastThreadDump = data.get(0);
        var it = data.iterator();
        it.next();
        while (it.hasNext()) {
            var threadDump = it.next();

            var elapsedTimeNanos = BigInteger.valueOf(threadDump.getTimeInNanos() - lastThreadDump.getTimeInNanos());

            threadDump.getThreadToStackTraceElement().forEach((threadName, stackTraceElements) -> {

                if (stackTraceElements.isEmpty()) {
                    return;
                }

                threadName = aggregateThreadPool(threadName);

                var tree = new AtomicReference<>(rootTree.compute(threadName));

                // Just connect edges
                for (int i = stackTraceElements.size() - 1; i >= 0; i--) {
                    var stackString = stackTraceElements.get(i).toString();

                    tree.set(tree.get().compute(stackString));
                }

                // Connect last with value
                var value = tree.get().getValue().orElse(BigInteger.ZERO);
                tree.get().setValue(value.add(elapsedTimeNanos));
            });

            lastThreadDump = threadDump;
        }

        return rootTree;
    }

    private String aggregateThreadPool(String threadName) {
        var aggregation = agentConfig.getAggregation();
        if (aggregation == null) {
            return threadName;
        }
        var threadsAggregation = aggregation.getThreads();
        if (threadsAggregation == null) {
            return threadName;
        }

        return threadsAggregation.stream()
                .map(pattern -> pattern.matcher(threadName))
                .filter(Matcher::matches)
                .filter(matcher -> matcher.groupCount() > 0)
                .map(matcher -> {
                    var sb = new StringBuilder();
                    for (int i = 1; i <= matcher.groupCount(); i++) {
                        sb.append(matcher.group(i));
                    }
                    return sb.toString();
                })
                .findFirst()
                .orElse(threadName);
    }
}
