package com.itfruit.jmcutils.tree;


import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

public class TreeWithValue<K, V> {
    private K key;
    private Map<K, TreeWithValue<K, V>> children = new HashMap<>();
    private AtomicReference<V> value;

    public TreeWithValue() {
    }

    private TreeWithValue(K key) {
        this.key = key;
    }

    public TreeWithValue<K, V> compute(K key) {
        return children.computeIfAbsent(key, (ignore) -> new TreeWithValue<>(key));
    }

    public void setValue(V value) {
        this.value = new AtomicReference<>(value);
    }

    public Optional<V> getValue() {
        return Optional.ofNullable(value).map(AtomicReference::get);
    }

    public void traverse(TraverseFunction<K, V> traverser) throws Exception {
        var iterator = new RootToLeafIterator();
        recursiveTraverse(this, iterator, traverser);
    }

    private void recursiveTraverse(TreeWithValue<K, V> tree, RootToLeafIterator rootIterator,
                                   TraverseFunction<K, V> traverser) throws Exception {
        var value = tree.getValue();
        if (value.isPresent()) {
            traverser.iteration(rootIterator, value.get());
            return;
        }

        for (K key : tree.children.keySet()) {
            var childrenTree = tree.children.get(key);

            var iterator = new RootToLeafIterator(rootIterator, childrenTree);

            recursiveTraverse(childrenTree, iterator, traverser);
        }
    }

    private K getKey() {
        return key;
    }

    /**
     * Merges provided tree into this tree
     *
     * @param otherTree the provided tree to be merged to this one
     */
    public void merge(TreeWithValue<K, V> otherTree, BiFunction<V, V, V> mergeFunction) {
        recursiveMerge(this, otherTree, mergeFunction);
    }

    private void recursiveMerge(TreeWithValue<K, V> thisTree, TreeWithValue<K, V> otherTree,
                                BiFunction<V, V, V> mergeFunction) {
        var value = otherTree.getValue();
        if (value.isPresent()) {
            var newValue = thisTree.getValue().map(thisTreeValue -> mergeFunction.apply(thisTreeValue, value.get()))
                    .orElse(value.get());
            thisTree.setValue(newValue);
            return;
        }

        for (K key : otherTree.children.keySet()) {
            var otherChildrenTree = otherTree.children.get(key);

            var childrenTree = thisTree.compute(key);

            recursiveMerge(childrenTree, otherChildrenTree, mergeFunction);
        }
    }

    @FunctionalInterface
    public interface TraverseFunction<K, V> {
        void iteration(Iterator<K> iterator, V value) throws Exception;
    }

    private class RootToLeafIterator implements Iterator<K> {

        private final List<K> keys;
        private final Iterator<K> it;

        RootToLeafIterator() {
            keys = List.of();
            it = keys.iterator();
        }

        @SuppressWarnings("unchecked")
        RootToLeafIterator(RootToLeafIterator rootIterator, TreeWithValue<K, V> tree) {
            var arr = (K[]) Arrays.copyOf(rootIterator.getKeys().toArray(), rootIterator.getKeys().size() + 1);
            arr[arr.length - 1] = tree.getKey();
            keys = Arrays.asList(arr);
            it = keys.iterator();
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public K next() {
            return it.next();
        }

        private List<K> getKeys() {
            return keys;
        }
    }
}
