package com.itfruit.jmcutils.config;

import lombok.Data;

import java.time.Duration;
import java.util.List;
import java.util.regex.Pattern;

@Data
public class AgentConfig {
    Long pid;
    String resultPath;
    Command command;
    Processing processing;

    Aggregation aggregation;
    Intervals intervals;
    Limits limits;

    @Data
    public static class Aggregation {
        List<Pattern> threads;
        List<Pattern> stacks;
    }

    @Data
    public static class Intervals {
        Duration threadDump;
        Duration aggregateDumpsToResults;
        List<Duration> aggregateRotateResults;
    }

    @Data
    public static class Limits {
        Integer rotateToZipAfterMegabytes;
        Duration stopAllWhenAggregateDumpsNotRunning;
        List<Pattern> include;
        List<Pattern> exclude;
    }

    public enum Command {
        start, stop, restart
    }

    public enum Processing {
        local, remote
    }
}
