package com.itfruit.jmcutils.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.regex.Pattern;

public class ConfigReader {
    private ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());

    public ConfigReader() {
        SimpleModule module = new SimpleModule();
        module.addSerializer(Duration.class, new DurationSerializer());
        module.addDeserializer(Duration.class, new BetterDurationDeserializer());
        module.addDeserializer(Pattern.class, new PatternDeserializer());

        yamlReader.registerModule(module);
        yamlReader.setPropertyNamingStrategy(PropertyNamingStrategy.KEBAB_CASE);
    }

    public <R> R readYamlFromInput(InputStream is, Class<R> type) throws IOException {
        return yamlReader.readValue(is, type);
    }

    public String toStringYaml(Object type) throws IOException {
        return yamlReader.writeValueAsString(type);
    }

    public static class DurationSerializer extends StdSerializer<Duration> {

        protected DurationSerializer() {
            super(Duration.class);
        }

        @Override
        public void serialize(Duration value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeString(value.toString());
        }
    }

    public static class BetterDurationDeserializer extends StdDeserializer<Duration> {

        public BetterDurationDeserializer() {
            super(Duration.class);
        }

        @Override
        public Duration deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            var value = p.getValueAsString()
                    .replaceAll(" ", "")
                    .toUpperCase();

            if (value.startsWith("P")) {
                return Duration.parse(value);
            }

            if (value.endsWith("D")) {
                return Duration.parse("P" + value);
            }
            return Duration.parse("PT" + value);
        }
    }

    public static class PatternDeserializer extends StdDeserializer<Pattern> {

        protected PatternDeserializer() {
            super(Pattern.class);
        }

        @Override
        public Pattern deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return Pattern.compile(p.getValueAsString());
        }
    }
}
