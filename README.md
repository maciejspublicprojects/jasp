## Java Attachable Sampler and Profiler
Low overhead profiler designed for **Enterprise** Java applications.

#### When to use
Applications, heavily relying on **data**, rather than processing, usually spending most of it's time
on operations to some sort of 3rdparty system (like Databases).

#### How is it working
This tool is using **threads and stack traces monitoring**. This is achieved by dumping all threads and 
it's stack traces at constant frequent intervals.<br/>
Gathered results are saved to the disk in **Flamegraph** flat format

#### Performance considerations
With 1000 threads and 20ms dump frequency, the overhead is up to one CPU core, **on the 
application that is being sampled.**<br/>
(Reduces linearly with less threads)

### Features
1. Tracking where your application spends it's time
2. Thread Pools aggregation to one
3. Thread filtering - whitelist and blacklist
4. Results in seconds, minutes, hours and days
5. Rotation to zip when results gets too big
6. Configuration on all of the above

#### Running
- `./gradlew clean build`
- `jcmd`
(To find the PID of the Java process)
- `./utils/agent/jagent.sh PID [start|stop|restart]`
  - see configuration `./utils/agent/agent_config.yml`

### Reading the results
The results will be located in path: `/tmp/PID`<br/>
Depending on yours intervals configuration you might see different result files in above directory.<br/>
All result files that are starting with `results-PT` are flamegraph results aggregated to different time intervals.<br/>
The ending file name characters are: S (seconds), M (minutes), H (hours), D (days)<br/>
i.e.: `results-PT1M` means that this result file will aggregate every 1 minute.

#### The Flamegraph GUI
I recommend opening in `speedscope` application: see https://github.com/jlfwong/speedscope<br/>
Results can be opened with locally installed `speedscope` app or by going to https://www.speedscope.app and pointing to the local file.<br/>
Note 1: The web application does not send the results data over the network, so confidentiality wise it's safe to use the web app.<br/>
Note 2: The application does not open too large files (300MB is like max). Try to open the aggregated ones (like in minutes).

#### Author
Maciej Zieniuk<br/>
Cloned from https://gitlab.com/maciejspublicprojects/jasp

#### License
**MIT**
